# A simple Semagrow-Cassandra Demo
## How to deploy
Firstly, start the Virtuoso and Cassandra containers:
```
$ docker-compose up -d virtuoso-store
$ docker-compose up -d cassandra-store
```
Then, insert the data in Cassandra using the following command:
```
$ docker-compose exec -d cassandra-store bash -c "cat /toLoad/books.cql | cqlsh"
```
Finally, start the SemaGrow container:
```
$ docker-compose up -d semagrow-cassandra
```
The metadata file needed for the Semagrow federator are already generated and placed in the metadata/ directory.
If you want to generate them yourself, issue the following commands before starting the Semagrow container:
```
$ docker-compose up -d scrape-virtuoso
$ docker-compose up -d scrape-cassandra
$ cat ./scraper/*.ttl > ./metadata/metadata.ttl
```
You can now issue queries on http://localhost:9001/SemaGrow/
If you want to execute a set of 5 simple queries in order to check the system and measure the performance,
you can issue the following command:   
```
$ docker-compose up query-evaluator
```
## The Federation
This Semagrow demo is a federation of two small datasets:

* A *Virtuoso triple-store*, which contains 10 triples. These triples are taken from dbpedia and contain some information (name, abstract and nationality) about 3 book writers.
* A *Cassandra store*, which contains two tables, one that consists of 14 rows and contains information about 14 books from these writers, and another table that consists of 25 rows that contain seller information about these books.

The Virtuoso data can be found under ./authors/ directory and the Cassandra data under ./books/ directory.

The Cassandra keyspace is the following:
```
CREATE TABLE book (
  author TEXT,
  isbn TEXT,
  title TEXT,
  year INT,
  publisher TEXT,
  PRIMARY KEY (author, title, isbn)
);
CREATE TABLE seller (
  isbn TEXT,
  name TEXT,
  price FLOAT,
  PRIMARY KEY (name, isbn)
);
```

The RDF mapping of Cassandra data is done as follows:

* Each Cassandra row corresponds to a unique subject uri, which is based on the values of primary key of the specific row.
* Each Cassandra column corresponds to a predicate uri.
* Each Cassandra cell corresponds to an object literal.

For example, the following row:

|author               |isbn           |title              |year|publisher       |
|---------------------|---------------|-------------------|----|----------------|
|'George R. R. Martin'|'0-553-10354-7'|'A Game of Thrones'|1996|'Bantam Spectra'|

is mapped as follows:
```
<http://cassandra.semagrow.eu/booksdb/book#title%3D%27A+Game+of+Thrones%27%3Bauthor%3D%27George+R.+R.+Martin%27%3Bisbn%3D%270-553-10354-7%27> 
    <http://cassandra.semagrow.eu/booksdb/book#author> 
    "George R. R. Martin" .
<http://cassandra.semagrow.eu/booksdb/book#title%3D%27A+Game+of+Thrones%27%3Bauthor%3D%27George+R.+R.+Martin%27%3Bisbn%3D%270-553-10354-7%27> 
    <http://cassandra.semagrow.eu/booksdb/book#isbn> 
    "0-553-10354-7" .
<http://cassandra.semagrow.eu/booksdb/book#title%3D%27A+Game+of+Thrones%27%3Bauthor%3D%27George+R.+R.+Martin%27%3Bisbn%3D%270-553-10354-7%27> 
    <http://cassandra.semagrow.eu/booksdb/book#title> 
    "A Game of Thrones" .
<http://cassandra.semagrow.eu/booksdb/book#title%3D%27A+Game+of+Thrones%27%3Bauthor%3D%27George+R.+R.+Martin%27%3Bisbn%3D%270-553-10354-7%27> 
    <http://cassandra.semagrow.eu/booksdb/book#year> 
    "1996"^^xsd:int .
<http://cassandra.semagrow.eu/booksdb/book#title%3D%27A+Game+of+Thrones%27%3Bauthor%3D%27George+R.+R.+Martin%27%3Bisbn%3D%270-553-10354-7%27> 
    <http://cassandra.semagrow.eu/booksdb/book#publisher> 
    "Bantam Spectra".
```
## Queries
In this section we will present some simple queries in order to demonstrate the capabilities of Semagrow federation engine.
#### Q1: All books written by George R. R. Martin.
Expected result set: 5 rows.
```
SELECT ?title ?isbn ?year WHERE {
  ?x <http://cassandra.semagrow.eu/booksdb/book#author> "George R. R. Martin" .
  ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
  ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  ?x <http://cassandra.semagrow.eu/booksdb/book#year> ?year .
}
```
This query must filter the author column in the book table of the Cassandra store. Since this column is a primary key column, this query can be evaluated directly in Cassandra. Therefore, the Semagrow decomposition process would transform the query as follows:
```
SELECT ?title ?isbn ?year WHERE {
  { ?x <http://cassandra.semagrow.eu/booksdb/book#author> "George R. R. Martin" .
    ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
    ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
    ?x <http://cassandra.semagrow.eu/booksdb/book#year> ?year .
  } @Cassandra
}
```
The actual evaluation process (after transforming the mapped SPARQL subquery to an actual CQL query) looks like this:
```
SELECT ?title ?isbn ?year WHERE {
  { SELECT title isbn year FROM book WHERE author="George R. R. Martin"; } @Cassandra
}
```
#### Q2: All books published in 1954.
Expected result set: 2 rows.
```
SELECT ?author ?title WHERE {
  ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
  ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
  ?x <http://cassandra.semagrow.eu/booksdb/book#year> "1954"^^xsd:int .
}
```
This query must filter the year column in the book table of the Cassandra store. Since this column is a regular column, this query cannot be evaluated directly in Cassandra. In this case, the Semagrow decomposition process has to insert a filtering operation in order to provide the desired results.
```
SELECT ?author ?title WHERE {
  { ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
    ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
    ?x <http://cassandra.semagrow.eu/booksdb/book#year> ?temp . 
  } @Cassandra
  FILTER (?temp = "1954"^^xsd:int )
}
```
The actual evaluation process (after transforming the mapped SPARQL subquery to an actual CQL query) looks like this:
```
SELECT ?author ?title WHERE {
  { SELECT author title temp FROM book; } @Cassandra
  FILTER (?temp = "1954"^^xsd:int )
}
```
#### Q3: All books that are priced between $7.0 and $12.0 and the seller name.
Expected result set: 14 rows.
```
SELECT ?title ?author ?seller ?price WHERE {
  ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
  ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
  ?x <http://cassandra.semagrow.eu/booksdb/book#year> ?temp .
  ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  ?y <http://cassandra.semagrow.eu/booksdb/seller#isbn> ?isbn .
  ?y <http://cassandra.semagrow.eu/booksdb/seller#price> ?price .
  ?y <http://cassandra.semagrow.eu/booksdb/seller#name> ?seller .
  FILTER (?price > 7.0 && ?price < 12.0)
}
```
This query actually joins data between the two Cassandra tables (i.e. book and seller tables). This query cannot run directly in Cassandra since the Cassandra API does not support joining operators between tables. In this case, Semagrow issues two source queries (one for each table) and performs the join operator in order to get the desired results. Also, since price is a regular column in seller table, Semagrow inserts a filtering operation as in the previous example. The decomposed query looks like this:
```
SELECT ?title ?author ?seller ?price WHERE {
  { ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
    ?x <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
    ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  } @Cassandra .
  { ?y <http://cassandra.semagrow.eu/booksdb/seller#isbn> ?isbn .
    ?y <http://cassandra.semagrow.eu/booksdb/seller#price> ?price .
    ?y <http://cassandra.semagrow.eu/booksdb/seller#name> ?seller .
  } @Cassandra
  FILTER (?price > 7.0 && ?price < 12.0)
}
```
The actual evaluation process (after transforming the mapped SPARQL subquery to an actual CQL query) looks like this:
```
SELECT ?title ?author ?seller ?price WHERE {
  { SELECT title isbn author FROM book; } @Cassandra .
  { SELECT price seller FROM book WHERE isbn IN ({?isbn}); } @Cassandra
  FILTER (?price > 7.0 && ?price < 12.0)
}
```
The Join operator in this case is actually a Bind Join operator, and in order to simulate the VALUES operator (that is not supported by CQL) we use the CQL IN operator.
#### Q4: All books written by american authors.
Expected result set: 11 rows.
```
SELECT ?author ?isbn ?year ?title WHERE {
  ?x <http://dbpedia.org/property/name> ?author .
  ?x <http://dbpedia.org/property/nationality> "American" .
  ?y <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
  ?y <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  ?y <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
  ?y <http://cassandra.semagrow.eu/booksdb/book#year> ?year .
}
```
In this example, a join between a Virtuoso triple store and a cassandra table is performed.
```
SELECT ?author ?isbn ?year ?title WHERE {
  { ?x <http://dbpedia.org/property/name> ?author .
    ?x <http://dbpedia.org/property/nationality> "American" .
  } @Virtuoso
  { ?y <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
    ?y <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
    ?y <http://cassandra.semagrow.eu/booksdb/book#title> ?title .
    ?y <http://cassandra.semagrow.eu/booksdb/book#year> ?year .
  } @Cassandra
}
```
#### Q5: All information about authors that have a book in amazon.
Expected result set: 2 rows.
```
SELECT DISTINCT ?author ?abstract ?nationality WHERE {
  ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
  ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  ?y <http://cassandra.semagrow.eu/booksdb/seller#isbn> ?isbn .
  ?y <http://cassandra.semagrow.eu/booksdb/seller#name> "amazon" .
  ?z <http://dbpedia.org/ontology/abstract> ?abstract .
  ?z <http://dbpedia.org/property/name> ?author .
  ?z <http://dbpedia.org/property/nationality> ?nationality .
}
```
In this example, a join between a Virtuoso triple store and two cassandra tables is performed. Notice that the name column is a primary key column and therefore the filtering is pushed on the store by the Semagrow decomposition process.
```
SELECT DISTINCT ?author ?abstract ?nationality WHERE {
  { ?z <http://dbpedia.org/ontology/abstract> ?abstract .
    ?z <http://dbpedia.org/property/name> ?author .
    ?z <http://dbpedia.org/property/nationality> ?nationality .
  } @Virtuoso . 
  { ?x <http://cassandra.semagrow.eu/booksdb/book#author> ?author .
    ?x <http://cassandra.semagrow.eu/booksdb/book#isbn> ?isbn .
  } @Cassandra . 
  { ?y <http://cassandra.semagrow.eu/booksdb/seller#isbn> ?isbn .
    ?y <http://cassandra.semagrow.eu/booksdb/seller#name> "amazon" .
  } @Cassandra
}
```